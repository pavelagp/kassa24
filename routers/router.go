package routers

import (
	"github.com/astaxie/beego"
	"kassa24/controllers"
)

func init() {
	beego.Router("/", &controllers.MainController{})
	beego.Router("/:id/timeslotes/:dt", &controllers.ReservPage{})
	beego.Router("/allrooms", &controllers.RoomsPage{})
	beego.Router("/auth", &controllers.AuthCont{})
	beego.Router("/set/:rid/:slid", &controllers.Setreserv{})
}
