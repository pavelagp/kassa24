package main

import (
	"github.com/astaxie/beego"
	_ "kassa24/routers"
	ut "kassa24/utilities"
)

func main() {
	go ut.Updatekeys()
	beego.Run()
}
