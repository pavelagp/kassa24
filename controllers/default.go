package controllers

import (
	"github.com/astaxie/beego"
	db "kassa24/models"
	ut "kassa24/utilities"
	"log"
	"strconv"
	"time"
)

type MainController struct {
	beego.Controller
}

func (c *MainController) Get() {
	c.Redirect("/allrooms", 302)
}

type AuthCont struct {
	beego.Controller
}

func (c *AuthCont) Get() {
	c.TplName = "login.html"
}

func (c *AuthCont) Post() {
	var (
		username, secret string
		user             db.AuthUser
		err              error
	)
	username = c.GetString("username")
	secret = c.GetString("secret")
	log.Println(username, secret)

	usrq := db.O.QueryTable("auth_user")
	err = usrq.Filter("email", username).One(&user)
	if err == nil && user.Password == secret {
		c.Ctx.SetCookie("token", user.AuthHash)
		c.Redirect("/", 302)

	}

	c.TplName = "login.html"
}

type ReservPage struct {
	beego.Controller
}

func (c *ReservPage) Get() {
	type record struct {
		Free       bool
		Slot       db.TimeSlot
		Reservator db.Reserv
	}

	var (
		slots   []db.TimeSlot
		reserv  db.Reserv
		records []record
	)
	pass, u := ut.Getuser(c.Ctx.GetCookie("token"))

	if pass {
		crid, err := strconv.Atoi(c.Ctx.Input.Param(":id"))
		day, err := strconv.Atoi(c.Ctx.Input.Param(":dt"))

		if err != nil {
			log.Println(err)
			c.Redirect("/allrooms", 302)
		} else {
			tss := db.O.QueryTable("time_slot")
			rsrv := db.O.QueryTable("reserv")
			room := db.Room{Id: crid}
			db.O.Read(&room)
			tss.All(&slots)
			t := time.Now()

			for _, slot := range slots {
				err = rsrv.Filter("conf_room_id", room.Id).Filter("reserv_date", t.AddDate(0, 0, day)).Filter("timeslot", slot).RelatedSel().One(&reserv)
				if err != nil {
					records = append(records, record{Free: true, Slot: slot})
				} else {
					log.Println(reserv)
					records = append(records, record{Free: false, Slot: slot, Reservator: reserv})
				}

			}

			c.Data["slots"] = records
			c.Data["room"] = room
			c.Data["next"] = day + 1
			c.Data["back"] = day - 1
			c.Data["now"] = t.AddDate(0, 0, day).Format("2006-01-02T15:04:05.00-07:00")
			c.Data["user"] = u

			c.TplName = "timeslotes.html"
		}
	} else {
		c.Redirect("/auth", 302)
	}
}

type RoomsPage struct {
	beego.Controller
}

func (c *RoomsPage) Get() {
	var (
		rooms []db.Room
	)
	pass, _ := ut.Getuser(c.Ctx.GetCookie("token"))
	log.Println(pass)

	if pass {
		rm := db.O.QueryTable("room")
		rm.All(&rooms)

		c.Data["rooms"] = rooms

		c.TplName = "confrooms.html"
	} else {
		c.Redirect("/auth", 302)
	}
}

type Setreserv struct {
	beego.Controller
}

func (c *Setreserv) Post() {
	var (
		reserv         db.Reserv
		err            error
		roomid, slotid int
	)
	type resp struct {
		Status int
	}

	pass, u := ut.Getuser(c.Ctx.GetCookie("token"))
	if pass {
		roomid, err = strconv.Atoi(c.Ctx.Input.Param(":rid"))
		slotid, err = strconv.Atoi(c.Ctx.Input.Param(":slid"))

		if err == nil {
			t := time.Now()
			z, _ := t.Zone()

			reserv.ConfRoom = &db.Room{Id: roomid}
			reserv.Owner = &u
			reserv.ReservDate, _ = time.Parse(time.RFC3339, c.GetString("date")+"T10:00:00"+z+":00")
			log.Println(reserv.ReservDate)
			reserv.Timeslot = &db.TimeSlot{Id: slotid}
			db.O.Insert(&reserv)
			var r resp
			r.Status = 1
			c.Data["json"] = &r
			c.ServeJSON()

		} else {
			c.Abort("403")
		}

	} else {
		c.Abort("403")
	}
}
