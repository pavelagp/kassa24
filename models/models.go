package models

import (
	"github.com/astaxie/beego/orm"
	_ "github.com/mattn/go-sqlite3"
	"time"
	//"log"
)

var (
	O orm.Ormer
)

type AuthUser struct {
	Id        int    `orm:"auto;pk"`
	Email     string `orm:"size(60);unique;index"`
	Firstname string `orm:"size(60)"`
	Lastname  string `orm:"size(60)"`
	Password  string `orm:"size(60)"`
	Active    bool   `orm:"default(false)"`
	AuthHash  string `orm:"size(64);unique;index"`
}

type Room struct {
	Id   int    `orm:"auto;pk"`
	Name string `orm:"size(60);unique;index"`
}

type TimeSlot struct {
	Id      int       `orm:"auto;pk"`
	Hour    int       `orm:"default(1)"`
	Reservs []*Reserv `orm:"reverse(many)"`
}

type Reserv struct {
	Id         int       `orm:"auto;pk"`
	ConfRoom   *Room     `orm:"rel(fk)"`
	Owner      *AuthUser `orm:"rel(fk)"`
	Timeslot   *TimeSlot `orm:"rel(fk)"`
	ReservDate time.Time `orm:"type(date)"`
	Creaded    time.Time `orm:"auto_now_add;type(datetime)"`
}

func init() {

	orm.RegisterDataBase("default", "sqlite3", "data.db", 30, 30)

	orm.RegisterModel(new(AuthUser), new(Room), new(TimeSlot), new(Reserv))

	//orm.RunSyncdb("default", true, true)
	//if err != nil {
	//    log.Println(err)
	//}
	O = orm.NewOrm()
	O.Using("default")

}
