package utilities

import (
	"crypto/sha256"
	"encoding/hex"
	db "kassa24/models"
	"log"
	"math/rand"
	"time"
)

func Keygen(n int) string {
	var letterRunes = []rune("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	rand.Seed(time.Now().UnixNano())
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func Updatekeys() {
	var users []db.AuthUser
	var user db.AuthUser
	forever := 0
	usrq := db.O.QueryTable("auth_user")
	for forever == 0 {
		rand.Seed(time.Now().UnixNano())
		usrq.All(&users)
		for _, user = range users {
			user.AuthHash = Keygen(64)
			db.O.Update(&user, "auth_hash")
		}
		time.Sleep(24 * time.Hour)
	}
}

func Getuser(key string) (bool, db.AuthUser) {
	var user db.AuthUser
	var err error
	log.Println(key)
	usrq := db.O.QueryTable("auth_user")
	err = usrq.Filter("auth_hash", key).One(&user)
	if err == nil {
		if user.Active == true {
			return true, user

		} else {
			return false, user
		}
	} else {
		log.Println(err)
		return false, user
	}

}

func CalculateHash(email, password string) string {
	record := email + password
	h := sha256.New()
	h.Write([]byte(record))
	hashed := h.Sum(nil)
	return hex.EncodeToString(hashed)
}
